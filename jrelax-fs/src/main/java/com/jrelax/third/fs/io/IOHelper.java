package com.jrelax.third.fs.io;

import java.io.File;

/**
 * IO帮助类
 * Created by zengc on 2016-12-25.
 */
public class IOHelper {
    public static String getProjectBaseDir(){
        String path = IOHelper.class.getResource("/").getPath();
        path = new File(path).getParentFile().getParentFile().getPath();
        return path;
    }

    public static String resolvePath(String location){
        location = location.replace("${project.baseDir}", getProjectBaseDir());
        return location;
    }
}
