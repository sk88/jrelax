package com.jrelax.third.fs;

import com.jrelax.third.fs.ftp.FTPService;
import com.jrelax.third.fs.http.HTTPService;
import com.jrelax.third.fs.rmi.RMIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Run implements Filter{
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	public static boolean starting = false;

	@Override
	public void destroy() {
		RMIService.getInstance().stop();
		FTPService.getInstance().stop();
		HTTPService.getInstance().stop();
	}
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException, ServletException {
		arg2.doFilter(arg0, arg1);
	}
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		StringBuilder sb = new StringBuilder();
		sb.append("\r\n======================================================================\r\n");
		sb.append("\r\n    欢迎使用 Jrelax-FS文件存储系统  - Powered By ZENGCHAO\r\n");
		sb.append("\r\n======================================================================\r\n");
		System.out.println(sb.toString());

		RMIService.getInstance().start();
		FTPService.getInstance().start();
		HTTPService.getInstance().start();

		starting = true;
		logger.info("FS所有服务已启动");
	}
}
